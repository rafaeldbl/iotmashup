FROM maven:latest
WORKDIR /usr/src/service
COPY pom.xml .
RUN mvn install
COPY . .
CMD ["mvn", "spring-boot:run", "-DmyActiveProfile=docker"]