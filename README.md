<p align="center">
  <a href="https://spring.io/projects/spring-boot">
    <img src="https://spring.io/img/homepage/icon-spring-boot.svg" alt="Logo" width=72 height=72>
  </a>

  <h3 align="center">Mashup API with Java 8 and SpringBoot 2</h3>

  <p align="center">
    This example app shows how to build a basic API with Java 8 and Spring Boot 2.
  </p>
</p>

## Author

* **Rafael Lima**  - [Linkedin](https://www.linkedin.com/in/rafaeldblima/)

## Quick start

1. Clone the project
 ```bash
 https://gitlab.com/rafaeldbl/iotmashup.git
 ```

2. Go inside project folder:
 ```bash
 cd iotmashup
 ```

3. Now we have two ways:
   1. Development
   
      **Warning**
      
      > Must have Java and Maven installed
  
      1. Install dependencies
  
       ```bash
       mvn install
       ```
       
      2. Configure Twitter API data in application-dev.properties
      
      > File inside /src/main/resources/

      3. Launch development server
  
       ```bash
       mvn spring-boot:run 
       ```
  
      4. Open browser
  
       ```bash
       http://localhost:8080/api
       ```
  
      5. Or you can use this link to see the endpoint documentation
  
       ```bash
       http://localhost:8080/swagger-ui.html
       ```
       
   2. Docker-compose
   
      **Warning**
      
      > Must have docker and docker-compose installed
      1. Go inside infra folder
  
       ```bash
       cd infra
       ```
      2. Make env file and configure the Twitter API data
  
       ```bash
       cp .env.example .env
       ```
  
      3. Launch docker
   
      **Warning**
      
      > build can take a while, please wait
         
      > port 8080 must be free to be used
  
       ```bash
       docker-compose up
       ```
  
      4. Open browser
  
       ```bash
       http://localhost:8080/api
       ```
  
      5. Or you can use this link to see the endpoint documentation
  
       ```bash
       http://localhost:8080/swagger-ui.html
       ```

## What's included

* Java 8
* SpringBoot 2;
* Swagger;
