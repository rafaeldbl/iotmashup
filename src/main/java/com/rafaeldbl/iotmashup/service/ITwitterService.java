package com.rafaeldbl.iotmashup.service;

import org.springframework.social.twitter.api.Tweet;

import java.io.IOException;
import java.util.List;

public interface ITwitterService {
    List<Tweet> tweetSearch(String searchString);

    List<Tweet> repoTweets(String projectName) throws IOException;
}
