package com.rafaeldbl.iotmashup.service.impl;

import com.rafaeldbl.iotmashup.model.ApiResponse;
import com.rafaeldbl.iotmashup.model.GithubProject;
import com.rafaeldbl.iotmashup.model.GithubProjectTweets;
import com.rafaeldbl.iotmashup.model.GithubResponse;
import com.rafaeldbl.iotmashup.service.IApiResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("ResponseService")
public class ApiResponseService implements IApiResponseService {

    @Autowired
    private GithubService githubService;

    @Autowired
    private TwitterService twitterService;

    @Override
    public ApiResponse getProjectsAndTweets(String searchParam) throws IOException {
        List<GithubProjectTweets> githubTweetsList = new ArrayList<>();
        GithubResponse githubResponse = githubService.getGithubRepos(searchParam);
        for (GithubProject project : githubResponse.getItems()) {
            GithubProjectTweets tweets = new GithubProjectTweets(project, twitterService.repoTweets(project.getName()));
            githubTweetsList.add(tweets);
        }
        return new ApiResponse(githubTweetsList, githubResponse.getTotalCount());
    }
}
