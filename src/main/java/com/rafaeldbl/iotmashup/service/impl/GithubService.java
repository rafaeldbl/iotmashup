package com.rafaeldbl.iotmashup.service.impl;

import com.rafaeldbl.iotmashup.model.GithubResponse;
import com.rafaeldbl.iotmashup.service.IGithubService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service("GithubService")
public class GithubService implements IGithubService {
    private static final String GITHUB_SORTING = "&sort=stars&order=desc&per_page=10";
    private static final String GITHUB_SEARCH_REPO_URI = "https://api.github.com/search/repositories?q=";

    public GithubResponse getGithubRepos(String searchParam) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(
                GITHUB_SEARCH_REPO_URI + searchParam + GITHUB_SORTING,
                GithubResponse.class
        );
    }
}
