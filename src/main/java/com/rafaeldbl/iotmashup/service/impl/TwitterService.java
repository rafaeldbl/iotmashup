package com.rafaeldbl.iotmashup.service.impl;


import com.rafaeldbl.iotmashup.config.TwitterConfig;
import com.rafaeldbl.iotmashup.service.ITwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service("TwitterService")
public class TwitterService implements ITwitterService {

    private static final int PAGE_SIZE = 10;
    private Twitter twitter;

    @Autowired
    TwitterConfig twitterConfig;

    public TwitterService(Twitter twitter) {
        this.twitter = twitter;
    }

    public List<Tweet> tweetSearch(String searchString) {
        SearchResults results = twitter.searchOperations().search(searchString, PAGE_SIZE);
        return results.getTweets();
    }

    private Twitter twitterTemplate() throws IOException {
        return twitterConfig.twitterTemplate();
    }

    public List<Tweet> repoTweets(String projectName) throws IOException {
        TwitterService twitterClient = new TwitterService(this.twitterTemplate());
        return twitterClient.tweetSearch(projectName);
    }
}
