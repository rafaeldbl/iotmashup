package com.rafaeldbl.iotmashup.service;

import com.rafaeldbl.iotmashup.model.ApiResponse;

import java.io.IOException;

public interface IApiResponseService {
    ApiResponse getProjectsAndTweets(String searchParam) throws IOException;
}
