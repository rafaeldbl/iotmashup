package com.rafaeldbl.iotmashup.service;

import com.rafaeldbl.iotmashup.model.GithubResponse;

public interface IGithubService {
    GithubResponse getGithubRepos(String searchParam);
}
