package com.rafaeldbl.iotmashup.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class TwitterConfig {

    private static final String DOCKER = "docker";

    @Value("${spring.profiles.active}")
    private String activeProfile;

    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
    private InputStream inputStream;

    @Bean
    public Twitter twitterTemplate() throws IOException {
        establishTwitterCreds();
        System.out.println(consumerKey + " * " + consumerSecret + "  *" + accessToken + " * " + accessTokenSecret);
        return new TwitterTemplate(consumerKey, consumerSecret, accessToken, accessTokenSecret);
    }

    private void establishTwitterCreds() throws IOException {

        try {
            Properties property = new Properties();
            if (this.activeProfile.equals(DOCKER)) {
                consumerKey = System.getenv("TWITTER_CONSUMER_KEY");
                consumerSecret = System.getenv("TWITTER_CONSUMER_SECRET");
                accessToken = System.getenv("TWITTER_ACCESS_TOKEN");
                accessTokenSecret = System.getenv("TWITTER_ACCESS_TOKEN_SECRET");
            } else {
                String appPropFile = "application-" + this.activeProfile + ".properties";
                inputStream = getClass().getClassLoader().getResourceAsStream(appPropFile);

                if (inputStream != null) {
                    property.load(inputStream);
                } else {
                    throw new FileNotFoundException("Application property file '" + appPropFile + "' not in the classpath.");
                }

                consumerKey = property.getProperty("twitter.consumerKey");
                consumerSecret = property.getProperty("twitter.consumerSecret");
                accessToken = property.getProperty("twitter.accessToken");
                accessTokenSecret = property.getProperty("twitter.accessTokenSecret");
                inputStream.close();
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

}
