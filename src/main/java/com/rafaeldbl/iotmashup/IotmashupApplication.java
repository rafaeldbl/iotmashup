package com.rafaeldbl.iotmashup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotmashupApplication {

	public static void main(String[] args) {
		SpringApplication.run(IotmashupApplication.class, args);
	}

}
