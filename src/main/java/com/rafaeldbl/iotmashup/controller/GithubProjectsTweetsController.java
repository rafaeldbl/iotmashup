package com.rafaeldbl.iotmashup.controller;

import com.rafaeldbl.iotmashup.model.ApiResponse;
import com.rafaeldbl.iotmashup.service.impl.ApiResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;

/**
 * Enpoint para busca de projetos do Github e seus respectivos tweets.
 *
 * @author Rafael Lima- rafaeldblima@gmail.com
 * @since 2019.03.16
 */
@Api(value = "Mashup Controller", description = "Endpoint para buscar projetos com seus tweets.")
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/api")
public class GithubProjectsTweetsController {
    private static final String GITHUB_REPO = "Internet of Things";

    private final ApiResponseService apiResponseService;

    @Autowired
    public GithubProjectsTweetsController(ApiResponseService apiResponseService) {
        this.apiResponseService = apiResponseService;
    }

    @ApiOperation("Get projects and tweets.")
    @RequestMapping(method = RequestMethod.GET, path = "")
    public ApiResponse searchGithubTwitter(
            @ApiParam(value = "searchParam", example = "Internet of Things", defaultValue = "Internet of Things")
            @RequestParam(value = "searchParam", required = false) Optional<String> searchParam
    ) throws IOException {
        String search = searchParam.orElse(GITHUB_REPO);
        return apiResponseService.getProjectsAndTweets(search);
    }
}

