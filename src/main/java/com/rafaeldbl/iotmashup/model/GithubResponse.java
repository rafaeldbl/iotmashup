package com.rafaeldbl.iotmashup.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GithubResponse {
    private int totalCount;
    private List<GithubProject> items;

    @JsonProperty("total_count")
    public int getTotalCount() {
        return this.totalCount;
    }

    @JsonProperty("total_count")
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    @JsonProperty("items")
    public List<GithubProject> getItems() {
        return this.items;
    }

    @JsonProperty("items")
    public void setItems(List<GithubProject> items) {
        this.items = items;
    }
}
