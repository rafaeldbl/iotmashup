package com.rafaeldbl.iotmashup.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.social.twitter.api.Tweet;

import java.util.List;

public class GithubProjectTweets {
    private GithubProject github;
    private List<Tweet> tweetList;

    public GithubProjectTweets(GithubProject github, List<Tweet> tweetList) {
        this.github = github;
        this.tweetList = tweetList;
    }

    @JsonProperty("github_repo")
    public GithubProject getGithub() {
        return github;
    }

    @JsonProperty("github_repo")
    public void setGithub(GithubProject github) {
        this.github = github;
    }

    @JsonProperty("tweets")
    public List<Tweet> getTweets() {
        return tweetList;
    }

    @JsonProperty("tweets")
    public void setTweets(List<Tweet> tweetList) {
        this.tweetList = tweetList;
    }
}
